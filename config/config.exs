# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :s3_direct_upload,
  ecto_repos: [S3DirectUpload.Repo]

# Configures the endpoint
config :s3_direct_upload, S3DirectUpload.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Rk1Ht5mysIXMNQTO0lAu+Fbh0cZMBMBS16TTeQQuBYuZ/uus+f/NJILU5JOxdO8p",
  render_errors: [view: S3DirectUpload.ErrorView, accepts: ~w(html json)],
  pubsub: [name: S3DirectUpload.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
