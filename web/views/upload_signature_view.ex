defmodule S3DirectUpload.UploadSignatureView do
  use S3DirectUpload.Web, :view

  def render("headers.json", %{signature: signature}) do
    signature
  end

end
