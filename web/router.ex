defmodule S3DirectUpload.Router do
  use S3DirectUpload.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", S3DirectUpload do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/api", S3DirectUpload do
    pipe_through :api

    get "/get_init_signature", UploadSignatureController, :get_init_signature
    get "/get_init_headers", UploadSignatureController, :get_init_headers
    get "/get_list_headers", UploadSignatureController, :get_list_headers
    get "/get_complete_headers", UploadSignatureController, :get_complete_headers
    get "/get_chunk_headers", UploadSignatureController, :get_chunk_headers
  end
end
