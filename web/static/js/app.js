// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

// import socket from "./socket"


$(document).ready(function() {
  var file, settings, uploader;

  $("#startUpload").attr("disabled", "disabled");
  $("#cancelUpload").attr("disabled", "disabled");

  $("#fileinput").on("change", function(element) {
    file = element.target.files[0];

    settings = {
      acl: "private",
      bucket: "finishfirstsoftware-video-upload",
      chunkHeadersPath: "/get_chunk_headers",
      completeHeadersPath: "/get_complete_headers",
      initHeadersPath: "/get_init_headers",
      listHeadersPath: "/get_list_headers",
      maxConcurrentChunks: 2,
      maxRetries: 1,
      region: "us-west-2",
      signatureBackend: "/api",
      ssl: true,
      uploaderFilePath: '/js/basic_s3_uploader.js',
      useWebWorkers: true,
      workerFilePath: '/js/basic_s3_worker.js',

      onReady: function() {
        $("#startUpload").removeAttr("disabled");
        $("#cancelUpload").attr("disabled", "disabled");
      },
      onStart: function() {
        $("#progress-container").show();
        $("#error").hide();
        $("#retries").hide();
        $("#progress-bar").removeClass("done").removeClass("error");
        $("#startUpload").attr("disabled", "disabled").text("Uploading...");
        $("#cancelUpload").removeAttr("disabled");
      },
      onProgress: function(loaded, total) {
        var progress = Math.ceil(((loaded / total) * 100));
        $("#progress-bar").text(progress + "%").css("width", progress + "%");
      },
      onComplete: function(location) {
        $("#progress-bar").text("DONE!").addClass("done");
        $("#startUpload").removeAttr("disabled").text("Upload it");
        $("#cancelUpload").attr("disabled", "disabled");
      },
      onRetry: function(attempt) {
        $("#retries").text("Retry #" + attempt).show();
      },
      onError: function(errorCode, description) {
        $("#error").text(description).show();
        $("#progress-bar").text("Upload failed!").css("width", "100%").addClass("error");
        $("#startUpload").removeAttr("disabled").text("Upload it");
        $("#cancelUpload").attr("disabled", "disabled");
      },
      onCancel: function() {
        $("#progress-bar").text("Upload cancelled").css("width", "100%").addClass("error");
        $("#startUpload").removeAttr("disabled").text("Upload it");
        $("#cancelUpload").attr("disabled", "disabled");
      }
    };
    uploader = new bs3u.Uploader(file, settings);
  });


  $("#startUpload").on("click", function() {
    uploader.startUpload();
  });

  $("#cancelUpload").on("click", function() {
    uploader.cancelUpload();
  });

});
