defmodule S3DirectUpload.PageController do
  use S3DirectUpload.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
