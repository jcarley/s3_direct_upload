defmodule S3DirectUpload.UploadSignatureController do
  use S3DirectUpload.Web, :controller

  def get_init_headers(conn, %{"host" => host, "key" => key, "content_type" => content_type, "acl" => acl, "payload" => payload, "region" => region}) do

    access_key = access_key()
    secret_access_key = secret_key()

    http_method = "POST"
    url = "#{host}/#{key}?uploads"

    headers = %{
      "content-type" => content_type,
      "x-amz-acl" => acl
    }

    # sign request
    signed_headers = AWSAuth.sign_authorization_header(access_key, secret_access_key,
      http_method,
      url,
      region,
      "s3",
      headers,
      payload)

    # convert to a Map, fix case of headers
    signature = Enum.reduce signed_headers, %{}, fn x, acc ->
      case x do
        {"host", _} ->
          acc
        {"Host", _} ->
          acc
        {key, value} ->
          Map.put(acc, key, value)
      end
    end

    render conn, "headers.json", signature: signature
  end

  def get_chunk_headers(conn, %{"host" => host, "key" => key, "content_type" => content_type, "upload_id" => upload_id, "payload" => payload, "part_number" => part_number, "region" => region}) do

    access_key = access_key()
    secret_access_key = secret_key()

    http_method = "PUT"
    url = "#{host}/#{key}?partNumber=#{part_number}&uploadId=#{upload_id}"

    headers = %{
      "content-type" => content_type
    }

    # sign request
    signed_headers = AWSAuth.sign_authorization_header(access_key, secret_access_key,
      http_method,
      url,
      region,
      "s3",
      headers,
      payload)

    signature = Enum.reduce signed_headers, %{}, fn x, acc ->
      case x do
        {"host", _} ->
          acc
        {"Host", _} ->
          acc
        {key, value} ->
          Map.put(acc, key, value)
      end
    end

    render conn, "headers.json", signature: signature
  end

  def get_list_headers(conn, %{"host" => host, "key" => key, "content_type" => content_type, "upload_id" => upload_id, "payload" => payload, "region" => region}) do

    access_key = access_key()
    secret_access_key = secret_key()

    http_method = "GET"
    url = "#{host}/#{key}?uploadId=#{upload_id}"

    headers = %{
      "content-type" => content_type
    }

    # sign request
    signed_headers = AWSAuth.sign_authorization_header(access_key, secret_access_key,
      http_method,
      url,
      region,
      "s3",
      headers,
      payload)

    signature = Enum.reduce signed_headers, %{}, fn x, acc ->
      case x do
        {"host", _} ->
          acc
        {"Host", _} ->
          acc
        {key, value} ->
          Map.put(acc, key, value)
      end
    end

    render conn, "headers.json", signature: signature
  end

  def get_complete_headers(conn, %{"host" => host, "key" => key, "content_type" => content_type, "upload_id" => upload_id, "payload" => payload, "region" => region}) do

    access_key = access_key()
    secret_access_key = secret_key()

    http_method = "POST"
    url = "#{host}/#{key}?uploadId=#{upload_id}"

    headers = %{
      "content-type" => content_type,
    }

    # sign request
    signed_headers = AWSAuth.sign_authorization_header(access_key, secret_access_key,
      http_method,
      url,
      region,
      "s3",
      headers,
      payload)

    signature = Enum.reduce signed_headers, %{}, fn x, acc ->
      case x do
        {"host", _} ->
          acc
        {"Host", _} ->
          acc
        {key, value} ->
          Map.put(acc, key, value)
      end
    end

    render conn, "headers.json", signature: signature
  end

  defp access_key() do
    System.get_env("AWS_ACCESS_KEY_ID")
  end

  defp secret_key() do
    System.get_env("AWS_SECRET_ACCESS_KEY")
  end

end

