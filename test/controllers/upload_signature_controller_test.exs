defmodule S3DirectUpload.UploadSignatureControllerTest do
  use S3DirectUpload.ConnCase

  test "it does shit", %{conn: conn} do

    http_method = "POST"
    url = "https://finishfirstsoftware-jcarley-uploads.s3.us-west-2.amazonaws.com/1479742160959_alleyson_felix.jpg?uploads"
    # url = "https://esp-prod-west.s3-us-west-2.amazonaws.com/submission/assets/batch_516303/9216391974469467_alleyson_felix.jpg?uploads"
    uri = URI.parse(url)

    payload = ""

    # content-type;host;x-amz-acl;x-amz-content-sha256;x-amz-date
    headers = [
      {"content-type", "image/jpeg"},
      {"host", uri.host},
      {"x-amz-acl", "private"},
      {"x-amz-content-sha256", AWS.Util.sha256_hexdigest(payload)}
    ]

    # credentials for IAM account: jcarley
    client = %{
      region: "us-west-2",
      service: "s3",
      access_key_id:     "AKIAI4Q7N7FJCZN6ZLPQ",
      secret_access_key: "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"
    }

    # sign request
    signed_headers = AWS.Request.sign_v4(client, http_method, url, headers, payload)
    # signed_headers = signed_headers|

    # Add two more headers
    # signature = signature
                # |> Map.put("X-Amz-Content-Sha256", AWS.Util.sha256_hexdigest(payload))

    case HTTPoison.post(url, payload, signed_headers) do
      {:ok, response=%HTTPoison.Response{status_code: 200, body: ""}} ->
        {:ok, nil, response}
      {:ok, %HTTPoison.Response{body: body, status_code: 403}} ->
        IO.puts body
      {:ok, %HTTPoison.Response{body: body, status_code: 400}} ->
        IO.puts body
    end

  end

  test "GET /get_init_headers", %{conn: conn} do
# %{"host" => host, "key" => key, "content_type" => content_type, "acl" => acl, "region" => region}) do
    params = %{
      host: "https://esp-dev-oregon.s3.amazonaws.com",
      key: "uploads/my-video.mov",
      content_type: "quicktime/mov",
      acl: "private",
      region: "us-west-2"
    }

    conn =
      get conn, upload_signature_path(conn, :get_init_headers), params
    response = json_response(conn, 200)

    assert response["Authorization"] =~ ~r/AWS4-HMAC-SHA256\s{1}Credential=[A-Za-z\/_0-9-]+,SignedHeaders=[A-Za-z\/_0-9-;]+,Signature=[0-9a-z]+/
    assert response["x-amz-date"]
    assert response["x-amz-content-sha256"]
    assert response["content-type"]
  end

  test "GET /get_list_headers", %{conn: conn} do

    params = %{
      host: "https://esp-dev-oregon.s3-us-west-2.amazonaws.com",
      key: "uploads/my-video.mov",
      content_type: "quicktime/mov",
      upload_id: "1"
    }

    conn =
      get conn, upload_signature_path(conn, :get_list_headers), params

    response = json_response(conn, 200)

    assert response["Authorization"] =~ ~r/AWS4-HMAC-SHA256\s{1}Credential=[A-Za-z\/_0-9-]+,SignedHeaders=[A-Za-z\/_0-9-;]+,Signature=[0-9a-z]+/
    assert response["x-amz-date"]
    assert response["x-amz-content-sha256"]
    assert response["content-type"]
  end

  test "GET /get_complete_headers", %{conn: conn} do

    params = %{
      host: "https://esp-dev-oregon.s3-us-west-2.amazonaws.com",
      key: "uploads/my-video.mov",
      content_type: "quicktime/mov",
      upload_id: "1",
      payload: "encrypted payload"
    }

    conn =
      get conn, upload_signature_path(conn, :get_complete_headers), params

    response = json_response(conn, 200)

    assert response["Authorization"] =~ ~r/AWS4-HMAC-SHA256\s{1}Credential=[A-Za-z\/_0-9-]+,SignedHeaders=[A-Za-z\/_0-9-;]+,Signature=[0-9a-z]+/
    assert response["x-amz-date"]
    assert response["x-amz-content-sha256"]
    assert response["content-type"]
  end

  test "GET /get_chunk_headers", %{conn: conn} do

    params = %{
      host: "https://esp-dev-oregon.s3-us-west-2.amazonaws.com",
      key: "uploads/my-video.mov",
      content_type: "quicktime/mov",
      upload_id: "1",
      payload: "encrypted payload",
      part_number: "1"
    }

    conn =
      get conn, upload_signature_path(conn, :get_chunk_headers), params

    response = json_response(conn, 200)

    assert response["Authorization"] =~ ~r/AWS4-HMAC-SHA256\s{1}Credential=[A-Za-z\/_0-9-]+,SignedHeaders=[A-Za-z\/_0-9-;]+,Signature=[0-9a-z]+/
    assert response["x-amz-date"]
    assert response["x-amz-content-sha256"]
    assert response["content-type"]
  end

end

